extends Node2D

export var window_scale : Resource

func _ready() -> void:
	WindowScaler.load(window_scale)

func _init() -> void:
	pass
