## godot-stuff Window Scaler

`Release 4.x-Rx (Works with Godot 4.x)`

A simple plugin for Godot to let the player change the Viewport Scale while playing. This can be programmed by the developer or use the provided Hot Key option to automatically change to the next scaled view.

### Pre-Requisites

- None

### How To Install

If you are using godot-stuff Project Manager (GSPM) then you can simply add the following line to your project.

```
assets:

  gs-windowscaler:
    location: https://gitlab.com/godot-stuff/gs-windowscalar.git
    includes:
      - dir: gs_windowscaler
```

For any other project, simple extract the source from the project and copy the folder "gs_windowscaler" to your project.

### How To Use

This plugin lets the developer create any number of Views that can be switched at runtime by the player. This is very useful when you want to control the Scale of the game when you are creating a pixel perfect game for example.

#### Setup Autoload

Add the window_scaler.gd to your Autoload list in Godot. A recommended name for your object is WindowScaler.

#### Setup Input Mapping

If you want to provide a Hot Key for switching viws you must map a key to the Action "next_view". This works with the "Use Hotkey" option.

#### Create View Data

Views are defined using the "window_view_resource.gd" and contains the following properties.

- Name -- the name of the view
- Width -- the horizontal resolution
- Height -- the vertical resolution
- Fullscreen -- is this in Fullscreen
- Default -- is this the default view

#### Create Scale Data

The Scale data sets a few options and also provides a place for you to add the views that you created above.

- Use Hotkey -- use the hotkey defined as "next_view" in the Input Mapping
- Use Default -- automatically open the default view
- Views -- an array containing the views you previously created

#### Load Scaler

Finally, during runtime, you need to load the Window Scaler with the Scale Data you defined to initialize it. This can simply be done following this example.

```
export (Resource) var window_scale

func _ready() -> void:
	WindowScaler.load(window_scale)
```
